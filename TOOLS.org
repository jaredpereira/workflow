* Tools
  Several different tools are important for my workflow, here I want to just
  document them, together with how I use them, and any related configuration.
* Org-Mode
  I use [[https://orgmode.org/][org-mode]] to write just about everything. I use it to take notes, write
  outlines, and to organize my todos (still getting the hang of that one).
  
  It's a simple markup format, combining hierarchical headers with many rich
  features and a great interface together with emacs.
** Capture Templates
   Capture templates are a major entry point to a lot of my workflow. They allow
   me to drop structured text into specific files with accessible shortcuts,
   wherever I am in emacs.

   #+begin_src elisp 
     ("l" "link" entry
      (file+headline "~/org/links.org" "Buffer")
      "* [[%^{url}][%^{title}]]")
     ("t" "task" entry
      (file+headline "~/org/tasks.org" "Buffer")
      "* TODO %^{title}
       %?")
     ("j" "journal" plain
      (file+olp+datetree "~/org/journal.org" )
      "    - %U: %?")
     ("i" "idea" entry
      (file+headline "~/org/notes/ideas.org" "Ideas")
      "* %?")
   #+end_src
   
** Problems
   I love org, but it does have it's set of problems. By far the main one is
   that it's extremely wed to emacs. There are some interesting projects
   building external parsers for it, orgasjs and [[https://github.com/ngortheone/org-rs][org-rs]], but they're pretty
   early stage. Other markup formats, like markdown or ReStructured Text, have
   a much wider adoption.
* Git
  I use git to version control all sorts of things, including this repo! I rely
  heavily on [[http://magit.vc/][magit]] to get things done, but I do head to the commandline from
  time to time.
  
  For my personal projects (like my blog and this repository) the git workflow
  is fairly minimal, but I do try to keep commits atomic to a single "feature".
  For collaborative projects the workflow is a little more elaborative,
  generally based on merge requests on gitlab. 
  
  I try to prioritize a clean commit log, though I only really occasionaly
  search through it.
* Grep/Ripgrep
  Since a lot of my work is in plain text grep let's me quickly search
  through them all. 
  
  Specifically I use [[https://github.com/BurntSushi/ripgrep][ripgrep]] because it seems to work "out of the box" better.
  
  It integrates into emacs together with projectile to allow me to quickly
  search the directories I'm in, and jump to the files where I need to. This is
  invaluable when trying to recall links or old notes. 
